﻿using UnityEngine;
using System.Collections;

public class ClearLinePiece : ClearablePiece {

	public bool isRow;


	public override void Clear()
	{
		base.Clear(); // Call the Clear() method from the base class, which is the ClearablePiece [the extended class]

		if (isRow)
		{
			piece.GridRef.ClearRow(piece.Y);
		}
		else
		{
			piece.GridRef.ClearColumn(piece.X);
		}
	}
}
